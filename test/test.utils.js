var assert  = require('assert');
var DSUtils = require('../struc/ds_utils');

var utils   = new DSUtils();

describe('Unit Test - Data Structure Utilities', function() {
    describe('initialization', function() {
        it('JSON data structure loaded', function() {
            assert.equal(true, utils.init());
        });
        it('get product list', function() {
            assert.ok(utils.getProducts());
        });
    });

    describe('test nike\'s data', function() {
        it('get nike\'s product list', function() {
            assert.ok(utils.getProducts('nike'));
        });
        it('nike has no group purchase for classic', function() {
            assert.ok(!utils.getProducts('nike').classic.group);
        });
        it('nike has discount for premium Ads', function() {
            assert.ok(utils.getProducts('nike').premium.discount);
        });
        it('nike special price for premium Ads is 379.99', function() {
            assert.equal(379.99, utils.getProducts('nike').premium.discount.price);
        });
    });

    describe('test ford\'s data', function() {
        it('get ford\'s product list', function() {
            assert.ok(utils.getProducts('ford'));
        });
        it('ford has group purchase for classic', function() {
            assert.ok(utils.getProducts('ford').classic.group);
        });
        it('ford has discount for standout', function() {
            assert.ok(utils.getProducts('ford').standout.discount);
        });
        it('nike has discount for premium Ads', function() {
            assert.ok(utils.getProducts('ford').premium.discount);
        });
        it('nike special price for premium Ads is 389.99', function() {
            assert.equal(389.99, utils.getProducts('ford').premium.discount.price);
        });
    });
});
