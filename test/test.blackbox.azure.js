var assert    = require('assert');
var expect    = require('chai').expect;
var Supertest = require('supertest');

var supertest = Supertest('http://checkoutapp.azurewebsites.net');


describe('Checkout calculation -> blackbox (azure)', function() {
    describe('request customer list', function() {
        it('should return a 200 response', function(done) {
            supertest.get('/customers')
            .set('Accept', 'application/json')
            .expect(200, done);
        });
        it('should return array of 4 items', function(done) {
            supertest.get('/customers')
            .set('Accept', 'application/json')
            .expect(200)
            .end(function(err, res) {
                expect(res.body.length).to.equal(4);
                done();
            });
        });
    });

    describe('client> default', function() {
        it('should return array of 4 items', function(done) {
            supertest.post('/calculate')
            .set('Accept', 'application/json')
            .send({ product_ids: ["classic", "standout", "premium"] })
            .expect(200)
            .end(function(err, res) {
                expect(Object.keys(res.body).length).to.equal(4);
                done();
            });
        });
        it('total for classic, standout, premium: $987.97', function(done) {
            supertest.post('/calculate')
            .set('Accept', 'application/json')
            .send({ product_ids: ["classic", "standout", "premium"] })
            .expect(200)
            .end(function(err, res) {
                expect(res.body.total).to.equal(987.97);
                done();
            });
        });
    });

    describe('client> unilever', function() {
        it('should return array of 3 items', function(done) {
            supertest.post('/calculate')
            .set('Accept', 'application/json')
            .send({ customer_id: 'unilever', product_ids: ["classic", "classic", "classic", "premium"] })
            .expect(200)
            .end(function(err, res) {
                expect(Object.keys(res.body).length).to.equal(3);
                done();
            });
        });
        it('total for classic, classic, classic, premium: $934.97', function(done) {
            supertest.post('/calculate')
            .set('Accept', 'application/json')
            .send({ customer_id: 'unilever', product_ids: ["classic", "classic", "classic", "premium"] })
            .expect(200)
            .end(function(err, res) {
                expect(res.body.total).to.equal(934.97);
                done();
            });
        });
    });

    describe('client> apple', function() {
        it('should return array of 3 items', function(done) {
            supertest.post('/calculate')
            .set('Accept', 'application/json')
            .send({ customer_id: 'apple', product_ids: ["standout", "standout", "standout", "premium"] })
            .expect(200)
            .end(function(err, res) {
                expect(Object.keys(res.body).length).to.equal(3);
                done();
            });
        });
        it('total for standout, standout, standout, premium: $1294.96', function(done) {
            supertest.post('/calculate')
            .set('Accept', 'application/json')
            .send({ customer_id: 'apple', product_ids: ["standout", "standout", "standout", "premium"] })
            .expect(200)
            .end(function(err, res) {
                expect(res.body.total).to.equal(1294.96);
                done();
            });
        });
    });

    describe('client> nike', function() {
        it('should return array of 2 items', function(done) {
            supertest.post('/calculate')
            .set('Accept', 'application/json')
            .send({ customer_id: 'nike', product_ids: ["premium", "premium", "premium", "premium"] })
            .expect(200)
            .end(function(err, res) {
                expect(Object.keys(res.body).length).to.equal(2);
                done();
            });
        });
        it('total for premium, premium, premium, premium: $1519.96', function(done) {
            supertest.post('/calculate')
            .set('Accept', 'application/json')
            .send({ customer_id: 'nike', product_ids: ["premium", "premium", "premium", "premium"] })
            .expect(200)
            .end(function(err, res) {
                expect(res.body.total).to.equal(1519.96);
                done();
            });
        });
    });
});
