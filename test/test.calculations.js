var assert  = require('assert');
var DSUtils = require('../struc/ds_utils');

var utils   = new DSUtils();

describe('Unit Test - Checkout calculation', function() {
    describe('initialization', function() {
        it('JSON data structure loaded', function() {
            assert.equal(true, utils.init());
        });
    });

    describe('client> default', function() {
        it('get product list', function() {
            assert.ok(utils.getProducts());
        });
        it('calculation for classic, standout, premium return 4 items', function() {
            var json = utils.getProducts();
            var tmp  = json.calculate(["classic", "standout", "premium"]);
            assert.equal(4, Object.keys(tmp).length);
        });
        it('total for classic, standout, premium: $987.97', function() {
            var json = utils.getProducts();
            var tmp  = json.calculate(["classic", "standout", "premium"]);
            assert.equal(987.97, tmp.total);
        });
    });

    describe('client> unilever', function() {
        it('get product list', function() {
            assert.ok(utils.getProducts('unilever'));
        });
        it('calculation for classic, classic, classic, premium return 3 items', function() {
            var json = utils.getProducts('unilever');
            var tmp  = json.calculate(["classic", "classic", "classic", "premium"]);
            assert.equal(3, Object.keys(tmp).length);
        });
        it('total for classic, classic, classic, premium: $934.97', function() {
            var json = utils.getProducts('unilever');
            var tmp  = json.calculate(["classic", "classic", "classic", "premium"]);
            assert.equal(934.97, tmp.total);
        });
    });

    describe('client> apple', function() {
        it('get product list', function() {
            assert.ok(utils.getProducts('apple'));
        });
        it('calculation for standout, standout, standout, premium return 3 items', function() {
            var json = utils.getProducts('apple');
            var tmp  = json.calculate(["standout", "standout", "standout", "premium"]);
            assert.equal(3, Object.keys(tmp).length);
        });
        it('total for standout, standout, standout, premium: $1294.96', function() {
            var json = utils.getProducts('apple');
            var tmp  = json.calculate(["standout", "standout", "standout", "premium"]);
            assert.equal(1294.96, tmp.total);
        });
    });

    describe('client> nike', function() {
        it('get product list', function() {
            assert.ok(utils.getProducts('nike'));
        });
        it('calculation for premium, premium, premium, premium return 2 items', function() {
            var json = utils.getProducts('nike');
            var tmp  = json.calculate(["premium", "premium", "premium", "premium"]);
            assert.equal(2, Object.keys(tmp).length);
        });
        it('total for premium, premium, premium, premium: $1519.96', function() {
            var json = utils.getProducts('nike');
            var tmp  = json.calculate(["premium", "premium", "premium", "premium"]);
            assert.equal(1519.96, tmp.total);
        });
    });
});
