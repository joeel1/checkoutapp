var express = require('express');
var router  = express.Router();


router.get('/customers', function(req, res, next) {
    res.setHeader("content-type", "application/json");
    if (router.ds_utils) {
        res.json(router.ds_utils.getCustomers());
    }
    else {
        res.status(404);
        res.json({ error: "data unavailable..." });
    }
    res.end();  
});

router.get('/products', function(req, res, next) {
    returnProducts('', req, res);
});

router.get('/products/:customer_id', function (req, res) {
    var customer_id = req.params.customer_id;
    returnProducts(customer_id, req, res);
});


router.post('/calculate', function(req, res, next) {
    var err_msg = "";
    var product = {};
    var body    = req.body;

    if (!router.ds_utils)
        err_msg = "data unavailable...";
    if (!body.product_ids)
      err_msg = "product id unavailable";
    if (!Array.isArray(body.product_ids))
      err_msg = "product id is not in array format";

    res.setHeader("content-type", "application/json");
    if (err_msg.length > 0) {
        res.status(404);
        res.json({ error: err_msg });
    }
    else {
        product = router.ds_utils.getProducts(body.customer_id);
        res.json(product.calculate(body.product_ids));
    }
    
    res.end();  
});

function returnProducts(customer_id, req, res) {
    res.setHeader("content-type", "application/json");
    if (router.ds_utils) {
        res.json(router.ds_utils.getProducts(customer_id));
    }
    else {
        res.status(404);
        res.json({ error: "data unavailable..." });
    }
    res.end();  
}

/* API manual. */
router.get('/', function (req, res) {
    var str  = "REST interface:\n\n"
    str     += "=========================================\n"
    str     += "get customer list\n"
    str     += "=========================================\n"
    str     += "method: GET\n"
    str     += "end point format:\n"
    str     += "/customers\n"
    str     += "\n"
    str     += "expected result:\n"
    str     += "[{\n"
    str     += "    \"id\": \"unilever\",\n"
    str     += "    \"name\": \"Unilever\"\n"
    str     += "  },\n"
    str     += "  {\n"
    str     += "    \"id\": \"apple\",\n"
    str     += "    \"name\": \"Apple\"\n"
    str     += "  },\n"
    str     += "  {\n"
    str     += "    \"id\": \"nike\",\n"
    str     += "    \"name\": \"Nike\"\n"
    str     += "  },\n"
    str     += "  {\n"
    str     += "    \"id\": \"ford\",\n"
    str     += "    \"name\": \"Ford\"\n"
    str     += "  }]\n"
    str     += "  \n"
    str     += "=========================================\n"
    str     += "get customer's products and promotions'\n"
    str     += "=========================================\n"
    str     += "method: GET\n"
    str     += "end point format:\n"
    str     += "/customers\n"
    str     += "/customers/<customer_id>\n"
    str     += " * product list, with no promotion will be returned, when customer id not provided.\n"
    str     += "\n"
    str     += "expected result:\n"
    str     += "{\n"
    str     += "  \"classic\": {\n"
    str     += "    \"name\": \"Classic Ad\",\n"
    str     += "    \"description\": \"Offers the most basic level of advertisement\",\n"
    str     += "    \"price\": 269.99,\n"
    str     += "    \"group\": {\n"
    str     += "      \"buy\": 5,\n"
    str     += "      \"pay\": 4\n"
    str     += "    }\n"
    str     += "  },\n"
    str     += "  \"standout\": {\n"
    str     += "    \"name\": \"Standout Ad\",\n"
    str     += "    \"description\": \"Allows advertisers to use a company logo and use a longer presentation text\",\n"
    str     += "    \"price\": 322.99,\n"
    str     += "    \"discount\": {\n"
    str     += "      \"price\": 309.99\n"
    str     += "    }\n"
    str     += "  },\n"
    str     += "  \"premium\": {\n"
    str     += "    \"name\": \"Premium Ad\",\n"
    str     += "    \"description\": \"Same benefits as Standout Ad, but also puts the advertisement at the top of the results, allowing higher visibility\",\n"
    str     += "    \"price\": 394.99,\n"
    str     += "    \"discount\": {\n"
    str     += "      \"price\": 389.99,\n"
    str     += "      \"minimum\": 3\n"
    str     += "    }\n"
    str     += "  }\n"
    str     += "}\n"
    str     += "\n"
    str     += "=========================================\n"
    str     += "calculate checkout items\n"
    str     += "=========================================\n"
    str     += "method: POST\n"
    str     += "end point format:\n"
    str     += "/calculate\n"
    str     += "\n"
    str     += "body format:\n"
    str     += "{\n"
    str     += "    \"customer_id\": \"unilever\",\n"
    str     += "    \"product_ids\": [\"classic\", \"classic\", \"classic\", \"premium\"]\n"
    str     += "}\n"
    str     += "\n"
    str     += "expected result:\n"
    str     += "{\n"
    str     += "  \"total\": 2713.91,\n"
    str     += "  \"classic\": {\n"
    str     += "    \"total\": 1349.95,\n"
    str     += "    \"details\": [\n"
    str     += "      \"group purchase: buy 3 pay 2 - 2 x $539.98 > $1079.96\",\n"
    str     += "      \"normal price: 1 x $269.99 > $269.99\"\n"
    str     += "    ]\n"
    str     += "  },\n"
    str     += "  \"standout\": {\n"
    str     += "    \"total\": 968.97,\n"
    str     += "    \"details\": [ \"normal price: 3 x $322.99\" ]\n"
    str     += "  },\n"
    str     += "  \"premium\": {\n"
    str     += "    \"total\": 394.99,\n"
    str     += "    \"details\": [ \"normal price: 1 x $394.99\" ]\n"
    str     += "  }\n"
    str     += "}\n"
    str     += "\n"
    res.setHeader("content-type", "text/plain");
    res.end(str);
});

module.exports = router;
