
> checkoutapp@0.0.0 test /Users/ariffin.ahmad/source_codes/azure/checkoutApp
> mocha



  Checkout calculation -> blackbox (azure)
    request customer list
      ✓ should return a 200 response (150ms)
      ✓ should return array of 4 items (196ms)
    client> default
      ✓ should return array of 4 items (40ms)
      ✓ total for classic, standout, premium: $987.97
    client> unilever
      ✓ should return array of 3 items (121ms)
      ✓ total for classic, classic, classic, premium: $934.97 (204ms)
    client> apple
      ✓ should return array of 3 items (206ms)
      ✓ total for standout, standout, standout, premium: $1294.96 (202ms)
    client> nike
      ✓ should return array of 2 items (190ms)
      ✓ total for premium, premium, premium, premium: $1519.96 (117ms)

  Checkout calculation -> blackbox (local)
    request customer list
      ✓ should return a 200 response
      ✓ should return array of 4 items
    client> default
      ✓ should return array of 4 items
      ✓ total for classic, standout, premium: $987.97
    client> unilever
      ✓ should return array of 3 items
      ✓ total for classic, classic, classic, premium: $934.97
    client> apple
      ✓ should return array of 3 items
      ✓ total for standout, standout, standout, premium: $1294.96
    client> nike
      ✓ should return array of 2 items
      ✓ total for premium, premium, premium, premium: $1519.96

  Unit Test - Checkout calculation
    initialization
      ✓ JSON data structure loaded
    client> default
      ✓ get product list
      ✓ calculation for classic, standout, premium return 4 items
      ✓ total for classic, standout, premium: $987.97
    client> unilever
      ✓ get product list
      ✓ calculation for classic, classic, classic, premium return 3 items
      ✓ total for classic, classic, classic, premium: $934.97
    client> apple
      ✓ get product list
      ✓ calculation for standout, standout, standout, premium return 3 items
      ✓ total for standout, standout, standout, premium: $1294.96
    client> nike
      ✓ get product list
      ✓ calculation for premium, premium, premium, premium return 2 items
      ✓ total for premium, premium, premium, premium: $1519.96

  Unit Test - Data Structure Utilities
    initialization
      ✓ JSON data structure loaded
      ✓ get product list
    test nike's data
      ✓ get nike's product list
      ✓ nike has no group purchase for classic
      ✓ nike has discount for premium Ads
      ✓ nike special price for premium Ads is 379.99
    test ford's data
      ✓ get ford's product list
      ✓ ford has group purchase for classic
      ✓ ford has discount for standout
      ✓ nike has discount for premium Ads
      ✓ nike special price for premium Ads is 389.99


  44 passing (2s)

