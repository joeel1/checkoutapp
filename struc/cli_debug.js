var DSUtils = require('./ds_utils');

var utils   = new DSUtils();

// print product
utils.init();



console.log("\n\n\n\n\n\n========================================================");
console.log("                       debug");
console.log("========================================================");
var json = utils.getProducts();
var tmp  = json.calculate(["classic", "standout", "premium"]);
console.log(">>>> default: " + JSON.stringify(tmp, null, "    "));

json = utils.getProducts('unilever');
tmp  = json.calculate(["classic", "classic", "classic", "classic", "premium"]);
console.log("\n\n>>>> unilever: " + JSON.stringify(tmp, null, "    "));

json = utils.getProducts('unilever');
tmp  = json.calculate(["classic", "classic", "classic", "premium"]);
console.log("\n\n>>>> unilever: " + JSON.stringify(tmp, null, "    "));

json = utils.getProducts('apple');
tmp  = json.calculate(["standout", "standout", "standout", "premium"]);
console.log("\n\n>>>> apple: " + JSON.stringify(tmp, null, "    "));

json = utils.getProducts('nike');
tmp  = json.calculate(["premium", "premium", "premium", "premium"]);
console.log("\n\n>>>> nike: " + JSON.stringify(tmp, null, "    "));

console.log("========================================================\n\n\n");