

var config_path = './products';

function DSUtils() { 
    this.json = {};
};

DSUtils.prototype.init = function() {
    var self = this;
    try {
        self.json = require(config_path);
        return true;
    } 
    catch (err) { 
        console.log("error loading " + config_path + "...\n > " + err); 
        return false;
    }
}


DSUtils.prototype.getCustomers = function() {
    return JSON.parse(JSON.stringify(this.json.clients));
}

DSUtils.prototype.getProducts = function(customer_id) {
    var tmp        = JSON.parse(JSON.stringify(this.json.products));
    var promotions = JSON.parse(JSON.stringify(this.json.promotions));
    var groups     = promotions.groups[customer_id];
    var discounts  = promotions.discounts[customer_id];

    if (tmp) {
        for (var key in tmp) {
            var product       = tmp[key];
            if (groups && groups[key])
                product.group = JSON.parse(JSON.stringify(groups[key]));
            if (discounts && discounts[key])
                product.discount = JSON.parse(JSON.stringify(discounts[key]));

            product.calculate = function(count) {
                if (this.group)
                    return calculateGroup(this, count);
                else if (this.discount)
                    return calculateDiscount(this, count);
                else
                    return calculateNormal(this, count);
            };
        };
        tmp.calculate      = function(ids) {
            var tmp_product = {};
            ids.forEach(function(id) {
                if (tmp_product[id])
                    tmp_product[id] = tmp_product[id] + 1;
                else
                    tmp_product[id] = 1;
            });

            var results = { "total": 0 };
            for (var key in tmp_product) {
                var product    = this[key];
                var result     = product.calculate(tmp_product[key]);
                results.total += result.total;
                results[key]   = result;
            }
            return results;
        };
    }
    return tmp;
}

DSUtils.prototype.toString = function() {
    return JSON.stringify(this.json, null, "    ");
}

function calculateGroup(product, count) {
    var group_count   = parseInt(count / product.group.buy);
    var balance_count = count % product.group.buy;

    var total_group   = group_count * product.group.pay * product.price;
    var total_balance = balance_count * product.price;
    var ret           =  { 
        "total":   total_group + total_balance,
        "details": ["group purchase: buy " + product.group.buy + " pay " + product.group.pay + " - " + group_count + " x $" + (product.group.pay * product.price) + " > $" + total_group] 
    };

    if (total_balance > 0)
        ret.details.push("normal price: " + balance_count + " x $" + product.price + " > $" + total_balance);

    return ret;
}

function calculateDiscount(product, count) {
    var flag_discounted = product.discount.minimum && count < product.discount.minimum ? false : true;

    return { 
        "total":   flag_discounted ? count * product.discount.price : count * product.price,
        "details": [flag_discounted ? "* discounted - " + count + " x $" + product.discount.price : "* discount doesnt apply - " + count + " x $" + product.price]
    };
}

function calculateNormal(product, count) {
    return { 
        "total":   count * product.price,
        "details": ["normal price: " + count + " x $" + product.price]
    };
}


module.exports = DSUtils;